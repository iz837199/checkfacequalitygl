import subprocess
import csv
import os
import sys  
import pandas as pd

# determine the image quality using OFIQ
def image_quality(image_folder):
    print(f'Calculating OFIQ quality scores for faces in folder: {image_folder}')
    
    # OFIQ app path
    exe_path = r'C:\Users\David_UoR\OFIQ-Project\install_x86_64\Release\bin\OFIQSampleApp.exe'
    
    # specify the parameters
    params = ['-c', r'C:\Users\David_UoR\OFIQ-Project\data\ofiq_config.jaxn', '-i', image_folder]
    
    # run the OFIQ app and capture the output
    process = subprocess.Popen([exe_path] + params, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    raw_scores = stdout.decode('utf-8')
    return(raw_scores)

# extract the raw and scalar quality score lists for each image
def get_quality_scores(raw_scores):
    
    # get the location of 'Filename'
    index = raw_scores.find('Filename')
    
    # return the substring from the start of raw_scores to 'Filename'
    if index != -1:
        return raw_scores[:index]
    
    # if 'Filename' is not found in the string, return the original string
    else:
        return raw_scores
    
# select the raw scores only and format into a dataframe
def format_quality_scores(quality_scores, camera, dataset_ID):
    
    # split the string into lines
    lines = quality_scores.split('\n')
    
    # initialize a list to store the data
    formatted_quality_scores = []
    
    # initialize image_file
    image_file = None
    
    # iterate over all lines
    for line in lines:
    
        # if the line starts with '-->' it is an image filename
        if line.startswith('-->'):
            image_file = line.split(': ')[1]
        
            # remove return character
            image_file = image_file.replace('\r', '')  
        
            # split the filename and face number
            base_filename, face_number = os.path.basename(image_file).rsplit('_face_', 1)
            base_filename = f'{camera}_{dataset_ID}_{base_filename}.png'
            face_number = face_number.replace('.png', '')
    
        # if the line starts with 'raw scores:' add the scores to formatted_quality_scores
        elif line.startswith('raw scores:'):    
            scores = line.split(':')[1].split(';')
            scores = [score.strip() for score in scores]
        
            # the image file is valid add the scores to formatted_quality_scores
            if image_file is not None:
                formatted_quality_scores.append([base_filename, face_number] + scores)
    
    # convert formatted_quality_scores to a DataFrame
    headers = [
        'Filename', 'Face', 'Unified Quality Score', 'Background Uniformity',
        'Illumination Uniformity', 'Luminance Mean', 'Luminance Variance',
        'Under Exposure Prevention', 'Over Exposure Prevention', 'Dynamic Range',
        'Sharpness', 'Compression Artifacts', 'Natural Colour',
        'Single Face Present', 'Eyes Open', 'Mouth Closed', 'Eyes Visible',
        'Mouth Occlusion Prevention', 'Face Occlusion Prevention',
        'Inter Eye Distance', 'Head Size', 'Leftward Crop Of The Face Image',
        'Rightward Crop Of The Face Image', 'Downward Crop Of The Face Image',
        'Upward Crop Of The Face Image', 'Head Pose Yaw', 'Head Pose Pitch',
        'Head Pose Roll', 'Expression Neutrality', 'No Head Coverings'
        ]            
    formatted_quality_scores = pd.DataFrame(formatted_quality_scores, columns = headers)
                
    return formatted_quality_scores

# merge the formatted quality scores and most similar images dataframes
def merge_most_similar(formatted_quality_scores, faces_folder):

    most_similar_references_path = os.path.join(faces_folder, 'most_similar_references.csv')
    
    # read the most similar references csv
    most_similar_references = pd.read_csv(most_similar_references_path)
    
    print (most_similar_references)
    # convert the 'Face' column in both dataframes to string type
    most_similar_references['Face'] = most_similar_references['Face'].astype(str)
    formatted_quality_scores['Face'] = formatted_quality_scores['Face'].astype(str)

    # merge the dataframes
    combined_results = pd.merge(most_similar_references, formatted_quality_scores, on=['Filename', 'Face'])
        
    return(combined_results)

### main program ###
if __name__ == '__main__':
    
    # loop through all cameras
    for camera in ['camera_1', 'camera_2', 'camera_3']:
        
        # loop through all datasets
        for i in range(1, 20):
            dataset_ID = str(i).zfill(2)
        
            # define the folder locations
            faces_folder = os.path.join('C:\\Users\\David_UoR\\OneDrive - University of Reading\\CSMPR21_data\\captured_faces\\'+ camera, dataset_ID)    
            
            # determine the OFIQ quality metrics for all images in the face folder
            raw_quality_data=image_quality(faces_folder)
        
            # extract the quality scores from the raw data
            quality_scores=get_quality_scores(raw_quality_data)
        
            # format the quality scores and put into a Pandas dataframe
            formatted_quality_scores = (format_quality_scores(quality_scores, camera, dataset_ID))
            print(formatted_quality_scores)
                        
            # merge the quality scores with the reference face matches and write to csv
            combined_results = merge_most_similar(formatted_quality_scores, faces_folder)
            combined_results.to_csv(os.path.join(faces_folder,'quality_scores.csv'), index=False)